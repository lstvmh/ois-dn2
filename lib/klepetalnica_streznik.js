var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var lastnostiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var kanali = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki, lastnostiGledeNaSocket);
    pridruzitevKanalu(socket, 'Skedenj', null);
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki, lastnostiGledeNaSocket);
    obdelajZasebnoSporocilo(socket, vzdevkiGledeNaSocket);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabniki', function() {
      var uporabnikiNaKanalu = io.sockets.clients(trenutniKanal[socket.id]);
      var uporabniki = [];
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        uporabniki.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
      }
      socket.emit('uporabniki', uporabniki);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki, lastnosti) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  lastnosti[socket.id] = {avt : generateNewFace()};
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek,
    lst: lastnosti[socket.id]
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal, geslo) {
  var canJoin = false;
  if(kanal in kanali) {
    if(kanali[kanal] == geslo) {
      canJoin = true;
    } else {
      if(kanali[kanal] != null) {
        socket.emit('pridruzitevOdgovor', {
          uspesno: false,
          sporocilo: 'Pridružitev v kanal ' + kanal + ' ni bilo uspešno, ker je geslo napačno!'
        })
      } else {
        socket.emit('pridruzitevOdgovor', {
          uspesno: false,
          sporocilo: 'Izbrani kanal ' + kanal + ' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev ' + kanal + ' ali zahtevajte kreiranje kanala z drugim imenom.'
        })
      }
    }
  } else {
    kanali[kanal] = geslo;
    canJoin = true;
  }
  if(canJoin) {
    socket.leave(trenutniKanal[socket.id]);
    socket.join(kanal);
    trenutniKanal[socket.id] = kanal;
    socket.emit('pridruzitevOdgovor', {
      uspesno: true,
      kanal: kanal
    });
    socket.broadcast.to(kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
    });
    var uporabnikiNaKanalu = io.sockets.clients(kanal);
    if (uporabnikiNaKanalu.length > 1) {
      var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (uporabnikSocketId != socket.id) {
          if (i > 0) {
            uporabnikiNaKanaluPovzetek += ', ';
          }
          uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        }
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sistemskoSporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki, lastnosti) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        lastnosti[socket.id] = {avt : generateNewFace()};
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          lst: lastnosti[socket.id]
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sistemskoSporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporociloAvatar', {
      avtor: vzdevkiGledeNaSocket[socket.id],
      besedilo: sporocilo.besedilo,
      lst: lastnostiGledeNaSocket[socket.id]
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    pridruzitevKanalu(socket, kanal.novKanal, kanal.geslo);
  });
}

function obdelajZasebnoSporocilo(socket, vzdevkiGledeNaSocket) {
  socket.on('zasebnoSporociloZahteva', function(podatki) {
    var id = null;
    for(var key in vzdevkiGledeNaSocket) {
      if(vzdevkiGledeNaSocket[key] == podatki.prejemnik) {
        id = key;
      }
    }
    if(socket.id == id) {
      socket.emit('zasebnoSporociloOdgovor', {
        uspesno: false,
        sporocilo: podatki.besedilo,
        prejemnik: podatki.prejemnik
      });
      return;
    }
    if(!id) {
      socket.emit('zasebnoSporociloOdgovor', {
        uspesno: false,
        sporocilo: podatki.besedilo,
        prejemnik: podatki.prejemnik
      });
    } else {
      socket.emit('zasebnoSporociloOdgovor', {
        uspesno: true,
        sporocilo: podatki.besedilo,
        prejemnik: podatki.prejemnik
      });
      io.sockets.socket(id).emit('zasebnoSporocilo', {
        posiljatelj: vzdevkiGledeNaSocket[socket.id],
        besedilo: podatki.besedilo
      });
    }
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}

function generateNewFace() {
  var lastnosti = [[]];
  for(var x = 0; x < 5; x++){
    lastnosti[x] = new Array(5);
    for(var y = 0; y < 5; y++){
      lastnosti[x][y] = getRndColor();
    }
  }
  return lastnosti;
}

function getRndColor() {
  var r = 255*Math.random()|0,
      g = 255*Math.random()|0,
      b = 255*Math.random()|0;
  return 'rgb(' + r + ',' + g + ',' + b + ')';
}