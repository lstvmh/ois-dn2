var filter = {};
var regEx = null;

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementUprTekst(sporocilo) {
  return $('<div style="font-size: 12px"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div class="notice"></div>').html('<span class="notice-body">' + sporocilo + '</span>');
}

function divElementSelfTekst(sporocilo) {
  return $('<div class="message-self message"><div class="message-self-body" style="font-weight: bold">' + sporocilo + '</div></div>');
}

function simboli(sporocilo) {
  var sml = {
    ':)' : 'smiley',
    ':(' : 'sad',
    ';)' : 'wink',
    ':*' : 'kiss',
    '(y)' : 'like'
  };
  var url = 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/';
  return sporocilo.replace(/(:\)|:\(|;\)|:\*|\(y\))+/g, function (u) {
    return sml[u] ? '<img src="' + url + sml[u] + '.png"/>' : u;
  });
}

function parseSwears() {
  $.get("../swearWords.csv", function(data) {
    var swears = data.split(/,/);
    var regular = "";
    for(var i = 0; i < swears.length; i++) {
      regular += "\\b" + swears[i] + "\\b|";
      var stars = "";
      for(var j = 0; j < swears[i].length; j++) {
        stars += "*";
      }
      filter[swears[i]] = stars;
    }
    regular = "(" + regular.substring(0, regular.length-1) + ")";
    regEx = new RegExp(regular,"g"); 
  });
}

function filterBesed(sporocilo) {
  return sporocilo.replace(regEx, function (w) {
    return filter[w] ? filter[w] : w;
  })
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    sporocilo = simboli(sporocilo);
    sporocilo = filterBesed(sporocilo);
    $('#sporocila').append(divElementSelfTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  parseSwears();
  
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      $('#uporabnik').text(rezultat.vzdevek + ' @ ');
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      var avt = generateFace(rezultat.lst.avt);
      $('#face-wrap-self').empty();
      $('#face-wrap-self').append(avt);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  socket.on('zasebnoSporociloOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      rezultat.sporocilo = simboli(rezultat.sporocilo);
      rezultat.sporocilo = filterBesed(rezultat.sporocilo);
      sporocilo = '(zasebno za ' + rezultat.prejemnik + '): ' + rezultat.sporocilo;
    } else {
      sporocilo = 'Sporočila "' + rezultat.sporocilo	+ '" uporabniku z vzdevkom "' + rezultat.prejemnik + '" ni bilo mogoče posredovati.';
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    if(rezultat.uspesno) {
      $('#kanal').text(rezultat.kanal);
      $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    } else {
      $('#sporocila').append(divElementHtmlTekst(rezultat.sporocilo));
    }
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });

  socket.on('sistemskoSporocilo', function (sporocilo) {
    var novElement = $('<div class="notice" style="font-weight: bold"></div>').html('<span class="notice-body">' + sporocilo.besedilo + '</span>');
    $('#sporocila').append(novElement);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  socket.on('sporociloAvatar', function (sporocilo) {
    sporocilo.besedilo = simboli(sporocilo.besedilo);
    sporocilo.besedilo = filterBesed(sporocilo.besedilo);
    var avtor = $('<div class="avtor"></div>').text(sporocilo.avtor);
    var novElement;
    if(!$('.message:last', $('#sporocila')).hasClass(sporocilo.avtor)) {
      novElement = $('<div class="message ' + sporocilo.avtor + '"></div>').html('<div class="message-body">' + sporocilo.besedilo + '</div>');
      var avatar = generateFace(sporocilo.lst.avt);
      novElement.prepend(avatar);
    } else {
      novElement = $('<div class="nvtm message ' + sporocilo.avtor + '"></div>').html('<div class="message-body">' + sporocilo.besedilo + '</div>');
    }
    novElement.append(avtor);
    $('#sporocila').append(novElement);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    novElement.children().transition({opacity: "0", visibility: "hidden", scale: 0, duration: 0, delay: 0});
    novElement.children('.cnvs-avt').transition({scale: 1.05, opacity: "1", visibility: "visible", duration: 300, delay: 1000 },100,"snap").transition({scale: 1, duration: 10, delay: 0},0,"snap");
    novElement.children('.message-body').transition({scale: 1, opacity: "1", visibility: "visible", duration: 400, delay: 1000 },100,"snap");
  });
  
  socket.on('zasebnoSporocilo', function (sporocilo) {
    sporocilo.besedilo = simboli(sporocilo.besedilo);
    sporocilo.besedilo = filterBesed(sporocilo.besedilo);
    var ins = sporocilo.posiljatelj + ' (zasebno): ' + sporocilo.besedilo;
    var novElement = $('<div style="font-weight: bold; color: #000FFF"></div>').html(ins);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();

    for(var i = 0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementUprTekst(uporabniki[i]));
    }
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
  
  $('#sporocila').on('click', '.cnvs-avt', function() {
    $(this).siblings('.avtor').transition({scale: 1.05, opacity: "1", visibility: "visible", duration: 300, delay: 0},100,"snap").transition({scale: 1, duration: 10, delay: 0},0,"snap");
    $(this).siblings('.avtor').transition({scale: 0, opacity: "0", visibility: "hidden", duration: 100, delay: 1500},100,"snap");
  });
});

function generateFace(arg) {
  var cvs = $('<canvas/>', {'class':'cnvs-avt'}).get(0);
  cvs.width = 36;
  cvs.height = 36;
  var ctx = cvs.getContext("2d");
  var diameter = cvs.width;
  var radius = diameter * 0.5;
  ctx.translate(0.5, 0.5);
  for(var x = 0; x < diameter; x += 8){
    for(var y = 0; y < diameter; y += 8){
      ctx.fillStyle = arg[x/8][y/8];
      ctx.fillRect(x, y, 9, 9);
    }
  }
  ctx.globalCompositeOperation = 'destination-in';
  ctx.arc(radius, radius, radius, 0, 2*Math.PI);
  ctx.fill();
  return cvs;
}
