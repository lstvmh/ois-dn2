var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal, geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.zasebnoSporocilo = function(prejemnik, vsebina) {
  this.socket.emit('zasebnoSporociloZahteva', {
    prejemnik: prejemnik,
    besedilo: vsebina
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      if(besede.length != 2) {
        var kanal = besede.join(' ');
        this.spremeniKanal(kanal, null);
      } else {
        if(besede[0][0] != '"' || besede[0][besede[0].length-1] != '"' || besede[1][0] != '"' || besede[1][besede[1].length-1] != '"') {
          var kanal = besede.join(' ');
          this.spremeniKanal(kanal, null);
        } else {
          var pkanal = besede[0].substring(1, besede[0].length-1);
          var geslo = besede[1].substring(1, besede[1].length-1);
          this.spremeniKanal(pkanal, geslo);
        }
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      if(besede.length != 2) {
        sporocilo = 'Neveljaven ukaz.';
      } else {
        if(besede[0][0] != '"' || besede[0][besede[0].length-1] != '"' || besede[1][0] != '"' || besede[1][besede[1].length-1] != '"') {
          sporocilo = 'Neveljaven ukaz.';
        } else {
          var prejemnik = besede[0].substring(1, besede[0].length-1);
          var vsebina = besede[1].substring(1, besede[1].length-1);
          this.zasebnoSporocilo(prejemnik, vsebina);
        }
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};